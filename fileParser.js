const textract = require("textract");

const path = "../assets/";

module.exports = req => {
  return new Promise((resolve, reject) => {
    textract.fromFileWithPath(path + req.query.filename, (err, text) => {
      if (err) {
        resolve({ status: false, error: err });
      }
      resolve({ status: true, data: text });
    });
  });
};
