const config = {
  func: require("../fileParser"),
  static: {
    name: "file-parser-service",
    port: "8002",
    method: "GET"
  }
};

module.exports = config;
