const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

class App {
  constructor(config) {
    this.config = config.static;
    this.func = config.func;
    this.express = express();
    this._middleware();
    this._router();
  }

  _middleware() {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(
      cors({
        origin: ["*"],
        methods: ["GET", this.config.method.toLowerCase()]
      })
    );
  }

  _router() {
    this.express.get("/", (req, res, next) => {
      res.send(`Running ${this.config.name} on port: ${this.config.port}.`);
    });

    this.express[this.config.method.toLowerCase()]("/api", (req, res, next) => {
      this.func(req).then(result => res.send(result));
    });
  }
}

module.exports = App;
